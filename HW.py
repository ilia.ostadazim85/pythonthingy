import unittest

class TestFizz(unittest.TestCase):
    def test_f(self):
        fizzBuzz(1)
    def test_f(self):
        self.assertEqual(fizzBuzz(3),"buzz")
    def test_a(self):
        self.assertEqual(fizzBuzz(10),"fizz")
    def test_l(self):
        self.assertEqual(fizzBuzz(53),"nope")
    def test_o(self):
        self.assertEqual(fizzBuzz(456),"buzz")
    def test_p(self):
        self.assertEqual(fizzBuzz(1000005),"buzzFizz")
    def test_j(self):
        self.assertEqual(fizzBuzz(4),"nope")
def fizzBuzz(a):
    if(a%3 == 0 and a%5 == 0):
        return "buzzFizz"
    if(a%3 == 0):
        return "buzz"
    elif(a%5 == 0):
        return "fizz"
    else:
        return "nope"
unittest.main()
